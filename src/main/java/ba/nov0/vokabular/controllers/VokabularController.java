package ba.nov0.vokabular.controllers;

import ba.nov0.vokabular.domain.Vokabular;
import ba.nov0.vokabular.services.VokabularService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class VokabularController {

    private final VokabularService vokabularService;

    @Autowired
    public VokabularController(VokabularService vokabularService) {
        this.vokabularService = vokabularService;
    }

    @RequestMapping("/search")
    public ResponseEntity searchVokabular(@RequestParam(value = "word", required = false) String word, @RequestParam(value = "lang", required = false) String lang) {
        if(StringUtils.isEmpty(word)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            List<Vokabular> results = vokabularService.searchWord(word, lang);
            return new ResponseEntity<>(results, HttpStatus.OK);
        }
    }

}
