package ba.nov0.vokabular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VokabularApp {

    public static void main(String[] args) {
        SpringApplication.run(VokabularApp.class, args);
    }
}
