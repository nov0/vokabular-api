package ba.nov0.vokabular.services;


import org.jsoup.nodes.Document;

public interface SearchService {

    Document makeRequest(String word);
}
