package ba.nov0.vokabular.services;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService{

    private static final Logger LOG = LogManager.getLogger(SearchService.class);
    private static final String SEARCH_ERROR = "Error while getting data from remote!";

    private static final int TIMEOUT = 30000;
    private static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";

    public Document makeRequest(String searchUrl) {
        Document document = null;
        try {
            Connection.Response response = Jsoup.connect(searchUrl)
                    .timeout(TIMEOUT)
                    .userAgent(USER_AGENT)
                    .followRedirects(true)
                    .method(Connection.Method.GET)
                    .execute();
            document = response.parse();
        } catch (Exception e) {
            LOG.error(SEARCH_ERROR);
        }
        return document;
    }

}
