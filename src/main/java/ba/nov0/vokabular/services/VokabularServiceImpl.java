package ba.nov0.vokabular.services;

import ba.nov0.vokabular.domain.Vokabular;
import ba.nov0.vokabular.utility.VokabularSelectors;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VokabularServiceImpl implements VokabularService {

    @Value("${vokabular.search.url}")
    private String vokabularSearchUrl;

    private final SearchService searchService;

    @Autowired
    public VokabularServiceImpl(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public List<Vokabular> searchWord(String word, String lang) {
        Document searchResult = searchService.makeRequest(createSearchUrl(word, lang));
        Elements results = searchResult.select(VokabularSelectors.RESULT_HIT.value());
        results.addAll(searchResult.select(VokabularSelectors.RESULTS.value()));
        return processResults(results);
    }

    private List<Vokabular> processResults(Elements results) {
        List<Vokabular> vokabulars = new ArrayList<>();
        for(Element result : results) {
            Vokabular vokabular = convertElementToVokabular(result);
            if(vokabular != null) {
                vokabulars.add(vokabular);
            }
        }
        return vokabulars;
    }

    private Vokabular convertElementToVokabular(Element result) {
        Vokabular vokabular = new Vokabular();
        vokabular.setWord(getStringValueFromElement(result, VokabularSelectors.RESULT_WORD.value()));
        vokabular.setOrigin(getStringValueFromElement(result, VokabularSelectors.RESULT_ORIGIN.value()));
        vokabular.setMeaning(getMeaning(result));
        return vokabular;
    }

    private String getStringValueFromElement(Element result, String selector) {
        String word = null;
        if(result != null) {
            word = result.select(selector).text();
        }
        return word;
    }

    private String getMeaning(Element result) {
        return result.textNodes().get(1).text().trim();
    }

    private String createSearchUrl(String word, String lang) {
        return String.format(vokabularSearchUrl, lang, word);
    }

}
