package ba.nov0.vokabular.services;


import ba.nov0.vokabular.domain.Vokabular;

import java.util.List;

public interface VokabularService {

    List<Vokabular> searchWord(String word, String lang);

}
