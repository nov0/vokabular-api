package ba.nov0.vokabular.utility;


public enum VokabularSelectors {

    RESULTS("p.rezultat"),
    RESULT_HIT("p.rezultat_pogodak"),
    RESULT_WORD("span.rezultat_rec"),
    RESULT_ORIGIN("span.rezultat_poreklo");

    private String value;

    VokabularSelectors(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

}
